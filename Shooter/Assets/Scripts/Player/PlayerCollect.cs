﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollect : MonoBehaviour {

	var itemCount int;
	function onTriggerEnter(CollectionBase : Colider) 
	{
		if (CollectionBase.tag=="Pick Up") 
		{
			Destroy(CollectionBase.gameObject);
			itemCount ++;
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
